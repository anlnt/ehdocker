#!/bin/sh
currentDir=`dirname "$0"`
docker start postgres
docker start redis
echo "start container"
docker start mainapp
sleep 3
$currentDir/sync.sh
$currentDir/watch.sh
