#!/bin/sh
currentDir=`dirname "$0"`
echo "copy code to container"
rsync -r -v -a --delete --exclude-from=$currentDir/rsyncignore /Users/admin/dev/learning/libraryapi/ libraryapi:/app
