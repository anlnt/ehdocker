#!/bin/bash
SOURCE_DIR=/Users/admin/dev/eh/frontend-core
FILE_NAME=$1
$SOURCE_DIR/node_modules/eslint/bin/eslint.js $SOURCE_DIR/$FILE_NAME --ext .js --ext .jsx --ignore-path $SOURCE_DIR/.gitignore --ignore-path $SOURCE_DIR/.eslintignore
