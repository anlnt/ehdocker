#!/bin/sh
currentDir=`dirname "$0"`
echo "start container"
docker start auth
sleep 3
$currentDir/sync.sh
docker exec auth bundle exec eh_protobuf start -c '/app/config/environment.rb'
